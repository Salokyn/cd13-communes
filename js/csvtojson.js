#!/usr/bin/env node

if (process.argv.length <= 2) {
      console.log("Usage: " + __filename + " CSV_FILE");
      process.exit(-1);
}

const csv=require('csvtojson');

var json={error:0};

csv({delimiter:";"})
.fromFile(process.argv[2])
.then((jsonObj)=>{
  // Sort array
  json.data=jsonObj.sort(function(a,b){
    if (a.id>b.id) return 1;
    else if (a.id<b.id) return -1;
    else return 0;
  });
  
  // Build text field
  json.data.forEach(function (item, index) {
    item.text=item.id+" | "+item.commune;
  });
  
  // Write output
  process.stdout.write(JSON.stringify(json));
})